<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => 'mg.malkky.com',
		'secret' => 'k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPetZ+DiJBt2CkChrYpJPhUm00SCDYq5trx4902BgqAzSLMnfrMicOsf+FfsoHzHT2KVU7BIMp5S+s/c09/Pm00BMICguuipO5ieMGIuqzQQ1lgBLbieJP5vj+KhFaj0G6+qhIgt073PA4a/GtsDrfnemCH/sMtALsMl2IQ7o6SQIDAQAB',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

];
